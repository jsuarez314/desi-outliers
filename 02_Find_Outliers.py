#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd

from tqdm import tqdm
import os
from glob import glob

import pylab as pl
pl.style.use('../plots.mplstyle')

path = './catalog'

#--- PARAMETERS
nn = 45
md = 1.0
me = 'cosine'
ll = 0.15
mm = 5

# To filter not brz files
def not_brz(files):
    tmp_files = []
    for file in files:
        if not('brz' in file):
            tmp_files.append(file)
    return tmp_files

# To filter just brz files
def just_brz(files):
    tmp_files = []
    for file in files:
        if ('brz' in file):
            tmp_files.append(file)
    return tmp_files

def compute(files):    
    pbar = tqdm(total=len(files))
    for file in files:
        #print(file)
        command = f'/global/homes/j/jfsuarez/.conda/envs/outdet/bin/python3.9 ./tools/aux_outliers.py -f {file} --nn {nn} --md {md} --me {me} --ll {ll} --mm {mm} --outpath {outpath}'
        os.system(command)
        #print(command)
        pbar.update()
    pbar.close()

files = np.sort(glob(f'{path}/*/*/*/*spec.csv'))
compute(files)