import numpy as np

def scale(data):
    N = len(data)
    data_s = np.zeros(N, dtype=object)
    for i in range(N):
        data_s[i] = (data[i]-np.mean(data[i]))/np.std(data[i])
    return data_s.tolist()