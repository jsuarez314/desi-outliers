import numpy as np
import pandas as pd
from pyfof import friends_of_friends as fof
from dadapy import Data
import os
from tqdm import tqdm

from tools.scaler import scale

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

plt.style.use('./tools/outliers.mplstyle')

def ifof(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=False, imagefile='draft', imagepath='./', log=True, color=''):
    
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, nf = fof_params[0], fof_params[1]
    
    df_umap = pd.read_pickle(f'./reduced/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./catalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./catalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    

    aobs = 1.0
    afit = 0.5
        
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    Z = np.array(df_spec['Z'])
    classes = np.unique(SPECTYPE)

    
    SPECTYPE[SPECTYPE == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2 
    SPECTYPE[SPECTYPE == 'QSO'] = 1
    SPECTYPE[SPECTYPE == 'STAR'] = 2

    SPECTYPE = np.array(SPECTYPE.tolist())
    colors = np.zeros(len(SPECTYPE)).astype(str)
    if color=='bw':
        colors[SPECTYPE==1] = "#333333"
        colors[SPECTYPE==0] = "#999999"
        colors[SPECTYPE==2] = "#EEEEEE"
    else:
        colors[SPECTYPE==0] = "#75bbfd"
        colors[SPECTYPE==1] = "#c20078"
        colors[SPECTYPE==2] = "#96f97b"
        
    
    markers = ["o", "+", "^"]
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)

    WAVE = np.array(df_wave['WAVE'])


    ## FOF algorithm to find the islands
    friends = fof(data, ll)

    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min = []
    spec_friends = []
    z_friends = []
    n_members = []
    for f in friends:
        if (len(f)>=nf) & (len(f)<len(data)*0.2):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f)) 
            
    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0
    if save == False:
        return friends_min, spec_friends, z_friends, len(friends_min), size, n_members, sum(n_members)*100/size, mean, std
        
        
    if log == True:
        print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,nf))

    if all_island == True:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        
        fig = plt.figure(figsize=(8,8), constrained_layout=True)

        for k, c in enumerate(classes):
            plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=0.5, label=c, alpha=0.5, rasterized=True)
        plt.legend(markerscale=15, fontsize=15)        
        plt.grid(alpha=0.5)
        plt.title('Night={}, Release={}, Survey={} \n Band={}, $L_l$={:.2f}, Islands={}, Outliers={}'.format(night, release.title(), survey.upper(), band.upper(),ll,len(friends_min), sum(n_members)),size=18)

        for i in range(len(friends_min)):
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black', rasterized=True)
            if log == True:
                pbar.update()
        if log == True:                
            pbar.close()

        xmin = min(XUMAP)-1
        xmax = max(XUMAP)+1
        ymin = min(YUMAP)-1
        ymax = max(YUMAP)+1        
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        if save==True:
            os.makedirs(f'{imagepath}',exist_ok=True)
            #plt.savefig('{}/{}.png'.format(imagepath,imagefile), bbox_inches='tight',  renderized=True)
            plt.savefig('{}/{}.pdf'.format(imagepath,imagefile), bbox_inches='tight')
        if plot==True:
            plt.show()
        plt.close()
        del fig

    else:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        for i in range(len(friends_min)):
            fig = plt.figure(figsize=(30,9), constrained_layout=True)
            gs = fig.add_gridspec(3,12)
            ax1 = fig.add_subplot(gs[0:,:3])

            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=0.5, label=c, alpha=0.5)

            plt.legend(markerscale=15, fontsize=16)
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{}, Outliers={}'.format(night, release.title(), survey.upper(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])), size=22)
            plt.scatter(XUMAP[friends_min[i]], YUMAP[friends_min[i]], s=5, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X', fontsize=24)
            plt.ylabel('Y', fontsize=24)            
            
            for k, ii in enumerate(np.random.choice(friends_min[i],3,replace=False)):
                ax2 = fig.add_subplot(gs[k:k+1,3:])

                plt.plot(WAVE,FLUX[ii], alpha=aobs, c='k')

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(int(np.array(df_spec['EXPID'])[ii]))
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=24)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)


                if (k == 0) or (k == 1):
                    plt.setp(ax2.get_xaxis(), visible=False)
                    plt.setp(ax2.get_xticklabels(), visible=False)
                if (k==1):
                    plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=25)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=25)
            if save:
                os.makedirs(f'{imagepath}',exist_ok=True)
                plt.savefig('{}/{}_both_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_both_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            if plot == True:
                plt.show()
            plt.close()
            del fig


            fig, ax = plt.subplots(4,1, figsize=(15,20), sharey=True, sharex=True)
            for k, ii in enumerate(np.random.choice(friends_min[i],4,replace=False)):
                plt.subplot(4,1,k+1)

                plt.plot(WAVE,FLUX[ii], alpha=aobs)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(np.array(df_spec['EXPID'])[ii])
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=14)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)

                plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=18)

            fig.suptitle('Band={}, $L_l$={:.2f}, Island={}/{}, Outliers={}'.format(band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),y=0.92,size=20)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=18)
            if save:
                plt.savefig('{}/{}_spec_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_spec_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            # plt.show()
            plt.close()
            del fig

            fig = plt.figure(figsize=(8,8), rasterized=True)
            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=3, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{},  Members={}'.format(night, release.title(), survey.upper(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),size=20)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=10, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.tight_layout()
            if save:
                plt.savefig('{}/{}_umap_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_umap_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
#             plt.show()
            plt.close()
            del fig
            if log == True:
                pbar.update()
        if log == True:
            pbar.close()
    
    del data, df_umap, df_spec, df_wave, WAVE, FLUX, SPECTYPE, Z
    #this function return, 
    #outliers by island, spec_type by island, z by island, N_islands, N_objects, N_outliers by island, mean N_outliers, std N_outliers
    return friends_min, spec_friends, z_friends, len(friends_min), size, n_members, sum(n_members)*100/size, mean, std
   
   
def ifof_stats(inpath, obs_params, reduc_params, fof_params):
    
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, nf = fof_params[0], fof_params[1]
    
    df_umap = pd.read_pickle(f'./{inpath}/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./catalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./catalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    

    aobs = 1.0
    afit = 0.5
        
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    Z = np.array(df_spec['Z'])
    classes = np.unique(SPECTYPE)   
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)

    WAVE = np.array(df_wave['WAVE'])

    ## FOF algorithm to find the islands
    friends = fof(data, ll)

    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min = []
    spec_friends = []
    z_friends = []
    n_members = []
    for f in friends:
        if (len(f)>=nf) & (len(f)<len(data)*0.2):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f)) 
            
    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0
       
    print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,nf))

    #this function return, 
    #outliers by island, spec_type by island, z by island, N_islands, N_objects, N_outliers by island, rate, mean N_outliers, std N_outliers
    return friends_min, spec_friends, z_friends, len(friends_min), size, n_members, sum(n_members)*100/size, mean, std
   
def ifof_den(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=False, imagefile='draft', imagepath='./', log=True, color=''):
    
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, mm = fof_params[0], fof_params[1]
    
    df_umap = pd.read_pickle(f'./finalreduced/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./finalcatalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./finalcatalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    

    aobs = 1.0
    afit = 0.5
        
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    Z = np.array(df_spec['Z'])
    classes = np.unique(SPECTYPE)

    
    SPECTYPE[SPECTYPE == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2 
    SPECTYPE[SPECTYPE == 'QSO'] = 1
    SPECTYPE[SPECTYPE == 'STAR'] = 2

    SPECTYPE = np.array(SPECTYPE.tolist())
    colors = np.zeros(len(SPECTYPE)).astype(str)
    if color=='bw':
        colors[SPECTYPE==1] = "#333333"
        colors[SPECTYPE==0] = "#999999"
        colors[SPECTYPE==2] = "#EEEEEE"
    else:
        colors[SPECTYPE==0] = "#75bbfd"
        colors[SPECTYPE==1] = "#c20078"
        colors[SPECTYPE==2] = "#96f97b"
        
    
    markers = ["o", "+", "^"]
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)

    WAVE = np.array(df_wave['WAVE'])

    #-------Compute density
    data_den = Data(data, verbose=False)
    data_den.compute_density_kstarNN()
    density = data_den.log_den
    
    ## FOF algorithm to find the islands
    friends = fof(data, ll)

    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min = []
    spec_friends = []
    z_friends = []
    n_members = []
    for f in friends:
        if (len(f)>=mm) & (len(f)<len(data)*0.2) & (np.average(density[f])<-6.5):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f))

    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0

    if log == True:
        print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,mm))
        print('{} outliers identified!'.format(sum(n_members)))        
    if save == False:
        return friends_min, spec_friends, z_friends, len(friends_min), size, n_members, sum(n_members)*100/size, mean, std

    if all_island == True:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        
        fig = plt.figure(figsize=(8,8), constrained_layout=True)

        for k, c in enumerate(classes):
            plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=0.5, label=c, alpha=0.5, rasterized=True)
        plt.legend(markerscale=15, fontsize=15)        
        plt.grid(alpha=0.5)
        plt.title('Night={}, Release={}, Survey={} \n Band={}, $L_l$={:.2f}, Islands={}, Outliers={}'.format(night, release.title(), survey.upper(), band.upper(),ll,len(friends_min), sum(n_members)),size=25)

        for i in range(len(friends_min)):
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black', rasterized=True)
            if log == True:
                pbar.update()
        if log == True:                
            pbar.close()

        xmin = min(XUMAP)-1
        xmax = max(XUMAP)+1
        ymin = min(YUMAP)-1
        ymax = max(YUMAP)+1        
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        if save==True:
            os.makedirs(f'{imagepath}',exist_ok=True)
            #plt.savefig('{}/{}.png'.format(imagepath,imagefile), bbox_inches='tight',  renderized=True)
            plt.savefig('{}/{}.pdf'.format(imagepath,imagefile), bbox_inches='tight')
        if plot==True:
            plt.show()
        plt.close()
        del fig

    else:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        for i in range(len(friends_min)):
            fig = plt.figure(figsize=(33,10))
            gs = fig.add_gridspec(3,15)
            ax1 = fig.add_subplot(gs[0:,:4])

            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=0.5, label=c, alpha=0.5, rasterized=True)

            plt.legend(markerscale=15, fontsize=18)
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{}, Outliers={}'.format(night, release.title(), survey.upper(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])), size=25)
            plt.scatter(XUMAP[friends_min[i]], YUMAP[friends_min[i]], s=5, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X', fontsize=24)
            plt.ylabel('Y', fontsize=24)            
            
            for k, ii in enumerate(np.random.choice(friends_min[i],3,replace=False)):
                ax2 = fig.add_subplot(gs[k:k+1,5:])

                plt.plot(WAVE,FLUX[ii], alpha=aobs, c='k', rasterized=True)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(int(np.array(df_spec['EXPID'])[ii]))
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=24)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)


                if (k == 0) or (k == 1):
                    plt.setp(ax2.get_xaxis(), visible=False)
                    plt.setp(ax2.get_xticklabels(), visible=False)
                if (k==1):
                    plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=25)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=25)
            if save:
                os.makedirs(f'{imagepath}',exist_ok=True)
                #plt.savefig('{}/{}_both_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_both_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            if plot == True:
                plt.show()
            plt.close()
            del fig


            fig, ax = plt.subplots(4,1, figsize=(15,20), sharey=True, sharex=True)
            for k, ii in enumerate(np.random.choice(friends_min[i],4,replace=False)):
                plt.subplot(4,1,k+1)

                plt.plot(WAVE,FLUX[ii], alpha=aobs, rasterized=True)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(np.array(df_spec['EXPID'])[ii])
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=14)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)

                plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=20)

            fig.suptitle('Band={}, $L_l$={:.2f}, Island={}/{}, Outliers={}'.format(band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),y=0.92,size=25)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=20)
            if save:
                #plt.savefig('{}/{}_spec_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_spec_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            # plt.show()
            plt.close()
            del fig

            fig = plt.figure(figsize=(8,8))
            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=3, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{},  Members={}'.format(night, release.title(), survey.upper(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])), size=30)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=10, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.tight_layout()
            if save:
                #plt.savefig('{}/{}_umap_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                plt.savefig('{}/{}_umap_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
#             plt.show()
            plt.close()
            del fig
            if log == True:
                pbar.update()
        if log == True:
            pbar.close()
    
    del data, df_umap, df_spec, df_wave, WAVE, FLUX, SPECTYPE, Z
    #this function return, 
    #outliers by island, spec_type by island, z by island, N_islands, N_objects, N_outliers by island, mean N_outliers, std N_outliers
    return friends_min, spec_friends, z_friends, len(friends_min), size, n_members, sum(n_members)*100/size, mean, std