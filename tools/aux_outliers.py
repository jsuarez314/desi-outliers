import pandas as pd
from ccatalog import compute_umap
from ccatalog import ifof
import argparse
import os
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--filename", help="filename of the raw file")
parser.add_argument("-n", "--nn", help="umap number of neighbours")
parser.add_argument("-d", "--md", help="umap minimal distance")
parser.add_argument("-m", "--me", help="umap metric")
parser.add_argument("-l", "--ll", help="fof linking lenght")
parser.add_argument("-M", "--mm", help="fof minimal members")
parser.add_argument("-o", "--outpath", help="Outpath")
args = parser.parse_args()

file = args.filename
nn = int(args.nn)
md = float(args.md)
me = args.me
ll = float(args.ll)
mm = int(args.mm)
outpath = args.outpath

umap_stats_file = f'./umap_stats_nn{nn}_md{md}_{me}_ll{ll}_mm{mm}_cum.csv'
columns = ['FILE','NIGHT','RELEASE','SURVEY','BAND','MEMORY[GB]','TIME','MEMBERS_IDS','MEMBERS_SPEC_TYPE','MEMBERS_Z',
           'N_ISLANDS','N_OBS','LEN_OBS','N_OUTLIERS', 'TOTAL_OUTLIERS' ,'RATE_OUTLIERS','MEAN_OUTLIERS','STD_OUTLIERS']

if os.path.isfile(umap_stats_file) == False:
    df = pd.DataFrame(columns=columns)
    df.to_pickle(umap_stats_file)
else:
    df = pd.read_pickle(umap_stats_file)

if ((file in np.array(df['FILE'])) == False):
    night = file.split('/')[-4]
    release = file.split('/')[-3]
    survey = file.split('/')[-2]
    band = file.split('/')[-1].split('_')[3]

    memory, t_time = compute_umap(file,nn,md,me)

    obs_params = (night,release,survey,band)
    reduc_params = (nn,md,me)
    fof_params = (ll,mm)

    iid, spec, z, n_is, n_obs, len_obs, n_out, total_out, rate_out, meann_out, stdn_out = \
    ifof(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=True, log=False)

    df_temp = pd.DataFrame(np.array([[file,night,release,survey,band,memory,t_time, iid,
                                      spec, z, n_is, n_obs, len_obs, n_out, total_out, rate_out, meann_out, stdn_out]], dtype=object), columns = columns)
    df = pd.concat([df,df_temp], ignore_index=True)
    df.to_pickle(umap_stats_file)

    del df_temp
else:
    None
    # print(f'{file} already computed!')
del df