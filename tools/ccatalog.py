import os
import numpy as np
from glob import glob
from astropy.io import fits
import pandas as pd
import desispec.io
import psutil
import time

from tqdm import tqdm

import umap
from dadapy import Data
from pyfof import friends_of_friends as fof
from matplotlib import pyplot as plt

def create(release, survey, night, tiles, globoutpath):
    new_releases = ['denali','everest','fuji','guadalupe','iron']
    notrr_releases = ['andes','blanc','cascades','denali']

    path = f'/global/cfs/cdirs/desi/spectro/redux/{release}/tiles'
    
    if release in new_releases:
        path = f'{path}/cumulative'
    
    #print(f'Creating catalog for {release} from {path}')
    
    outpath = f'{globoutpath}/{night}/{release}/{survey}'    

    spec_file_b   = f'{outpath}/{night}_{release}_{survey}_b_spec.csv'
    spec_file_r   = f'{outpath}/{night}_{release}_{survey}_r_spec.csv'
    spec_file_z   = f'{outpath}/{night}_{release}_{survey}_z_spec.csv'
    spec_file_brz = f'{outpath}/{night}_{release}_{survey}_brz_spec.csv'
    
    wave_file_b   = f'{outpath}/{night}_{release}_{survey}_b_wave.csv'
    wave_file_r   = f'{outpath}/{night}_{release}_{survey}_r_wave.csv'
    wave_file_z   = f'{outpath}/{night}_{release}_{survey}_z_wave.csv'
    wave_file_brz = f'{outpath}/{night}_{release}_{survey}_brz_wave.csv'

    scolumns = ['TARGETID','TILEID','PETAL_LOC','EXPID','FIBER','MJD','TARGET_RA','TARGET_DEC','FIBERSTATUS',
                'FLUX_B','FLUX_R','FLUX_Z','FLUX_BRZ','SUMFLUX_B','SUMFLUX_R','SUMFLUX_Z','SUMFLUX_BRZ']
    zcolumns = ['TARGETID','Z','ZERR','ZWARN','SPECTYPE','CHI2','DELTACHI2']

    df_spec     = pd.DataFrame()
    df_wave_b   = pd.DataFrame()
    df_wave_r   = pd.DataFrame()
    df_wave_z   = pd.DataFrame()
    df_wave_brz = pd.DataFrame()

    if ( not(os.path.isfile(spec_file_b)) or not(os.path.isfile(wave_file_b)) or not(os.path.isfile(spec_file_r)) or not(os.path.isfile(wave_file_r)) or not(os.path.isfile(spec_file_z)) or not(os.path.isfile(wave_file_z)) or not(os.path.isfile(spec_file_brz)) or not(os.path.isfile(wave_file_brz)) ):
        
      
        for tile in tiles:

            #print(f'Finding files on {path}/{tile}/{night}/')
            sfiles = np.sort(glob(f'{path}/{tile}/{night}/spectra*.fits*'))
            if (sfiles !=0):
                os.makedirs(outpath, exist_ok=True) 
             
            #print(f'{path}/{tile}/{night}/spectra*.fits*')
            for sfile in sfiles:

                try:
                    sp = desispec.io.read_spectra(sfile)
                    sp_pd = sp.fibermap.to_pandas()

                    sp_pd['FLUX_B'] = [i for i in sp.flux['b']]
                    sp_pd['FLUX_R'] = [i for i in sp.flux['r']]
                    sp_pd['FLUX_Z'] = [i for i in sp.flux['z']]
                    sp_pd['FLUX_BRZ'] = [np.concatenate([x,y,z])  for x,y,z in zip(sp.flux['b'],sp.flux['r'],sp.flux['z'])]

                    sum_flux_b = [sum(i) for i in sp_pd['FLUX_B']]
                    sum_flux_r = [sum(i) for i in sp_pd['FLUX_R']]
                    sum_flux_z = [sum(i) for i in sp_pd['FLUX_Z']]
                    sum_flux_brz = [sum(i) for i in sp_pd['FLUX_BRZ']]

                    sp_pd['SUMFLUX_B'] = sum_flux_b
                    sp_pd['SUMFLUX_R'] = sum_flux_r
                    sp_pd['SUMFLUX_Z'] = sum_flux_z
                    sp_pd['SUMFLUX_BRZ'] = sum_flux_brz

                    if release in notrr_releases:
                        zfile = sfile.replace('spectra', 'zbest')
                    else:
                        zfile = sfile.replace('spectra', 'redrock')
                        zfile = zfile.replace('.gz', '')
                    zs = fits.open(zfile)

                    s_temp = sp_pd[scolumns]
                    z_temp = pd.DataFrame(zs[zcolumns], columns=zcolumns)

                    df_spec = pd.concat([df_spec, pd.merge(s_temp, z_temp)], ignore_index=True)

                except:
                    print(f'Error reading {sfile}')

        if len(df_spec) > 0:
            df_spec = df_spec[~df_spec.astype(str).duplicated()]

            df_spec = df_spec[df_spec['FIBERSTATUS'] == 0]
            df_spec = df_spec.drop(['FIBERSTATUS'], axis=1)
            
            df_spec_b = df_spec[df_spec['SUMFLUX_B'] != 0]
            df_spec_b = df_spec_b.drop(['SUMFLUX_R','SUMFLUX_Z','SUMFLUX_BRZ','FLUX_R','FLUX_Z','FLUX_BRZ'], axis=1)
            df_spec_b = df_spec_b.rename(columns = {'FLUX_B':'FLUX'})
            df_spec_b.to_pickle(spec_file_b)
            del df_spec_b
            
            df_spec_r = df_spec[df_spec['SUMFLUX_R'] != 0]
            df_spec_r = df_spec_r.drop(['SUMFLUX_B','SUMFLUX_Z','SUMFLUX_BRZ','FLUX_B','FLUX_Z','FLUX_BRZ'], axis=1)
            df_spec_r = df_spec_r.rename(columns = {'FLUX_R':'FLUX'})
            df_spec_r.to_pickle(spec_file_r)
            del df_spec_r
            
            df_spec_z = df_spec[df_spec['SUMFLUX_Z'] != 0]
            df_spec_z = df_spec_z.drop(['SUMFLUX_B','SUMFLUX_R','SUMFLUX_BRZ','FLUX_B','FLUX_R','FLUX_BRZ'], axis=1)
            df_spec_z = df_spec_z.rename(columns = {'FLUX_Z':'FLUX'})
            df_spec_z.to_pickle(spec_file_z)
            del df_spec_z
            
            df_spec_brz = df_spec[df_spec['SUMFLUX_BRZ'] != 0]
            df_spec_brz = df_spec_brz.drop(['SUMFLUX_B','SUMFLUX_R','SUMFLUX_Z','FLUX_B','FLUX_R','FLUX_Z'], axis=1)
            df_spec_brz = df_spec_brz.rename(columns = {'FLUX_BRZ':'FLUX'})
            df_spec_brz.to_pickle(spec_file_brz)
            del df_spec_brz
            
            try:
                df_wave_b['WAVE'] = sp.wave['b']
                df_wave_r['WAVE'] = sp.wave['r']
                df_wave_z['WAVE'] = sp.wave['z']
                df_wave_brz['WAVE'] = np.concatenate([sp.wave['b'],sp.wave['r'],sp.wave['z']])
            except:
                print('Wave error')
                None
            
            df_wave_b.to_pickle(wave_file_b)
            df_wave_r.to_pickle(wave_file_r)
            df_wave_z.to_pickle(wave_file_z)
            df_wave_brz.to_pickle(wave_file_brz)


            del sp, sp_pd, zs, s_temp, z_temp, df_spec, df_wave_b, df_wave_r, df_wave_z, df_wave_brz
                    
    return 0


def compute_umap(file, nn, md, me):
    
    max_size = 4000000000
    night = file.split('/')[2]
    release = file.split('/')[3]
    survey = file.split('/')[4]
    label = file.split('/')[-1][:-4]
    band = label.split('_')[-2]

    relpath = '{}/{}/{}'.format(night,release,survey)

    # try:
    umapfile = f'./reducedcum/{relpath}/{label}_nn{nn}_md{md}_{me}.csv'
    figfile = f'./figurescum/{relpath}/{label}_nn{nn}_md{md}_{me}'

    if not(os.path.isdir(f'./reducedcum/{relpath}')):
        os.makedirs(f'./reducedcum/{relpath}', exist_ok=True)
    if not(os.path.isdir(f'./figurescum/{relpath}')):
        os.makedirs(f'./figurescum/{relpath}', exist_ok=True)
    
    memory, t_time = 0, 0
    if ((os.path.isfile(umapfile) == False)):
        fsize = os.stat(file).st_size
        if fsize > max_size:
            print('Max size {}'.format(file))
            # Max_sizefiles.append(file)
        FLUX = list(pd.read_pickle(file)['FLUX'])

        # print('Computing Embedding\n')
        start_time = time.monotonic()
        reducer = umap.UMAP(n_neighbors=nn, min_dist=md, metric=me, random_state=42, low_memory=False)
        reducer.fit(FLUX)
        embedding = reducer.transform(FLUX)
        memory = psutil.Process().memory_info().rss / (1024 * 1024) /1000.0
        t_time = time.monotonic() - start_time
        #print(t_time)
        del reducer, FLUX

        df_UMAP = pd.DataFrame()
        df_UMAP['X_UMAP'] = embedding[:,0]
        df_UMAP['Y_UMAP'] = embedding[:,1]
        df_UMAP.to_pickle(umapfile)
        del df_UMAP

        Y1 = np.array(pd.read_pickle(file)['SPECTYPE'])

        classes = np.sort(np.unique(Y1))

        Y1[Y1 == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2
        Y1[Y1 == 'QSO'] = 1
        Y1[Y1 == 'STAR'] = 2

        colors = np.zeros(len(Y1)).astype(str)
        colors[Y1==0] = "#75bbfd"
        colors[Y1==1] = "#c20078"
        colors[Y1==2] = "#96f97b"

        fig = plt.figure(figsize=(20,6))
        xmin = min(embedding[:,0])-1
        xmax = max(embedding[:,0])+1
        ymin = min(embedding[:,1])-1
        ymax = max(embedding[:,1])+1

        size = 2.5

        plt.subplot(141)
        plt.title("All",size=20)
        plt.scatter(embedding[:,0], embedding[:,1], c=colors, cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid(alpha=0.5)

        plt.subplot(142)
        plt.title("Galaxy",size=20)
        plt.scatter(embedding[:,0][Y1==0], embedding[:,1][Y1==0], color="#75bbfd", cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)  
        plt.xlabel('X')                         
        plt.grid(alpha=0.5)

        plt.subplot(143)
        plt.title("QSO",size=20)
        plt.scatter(embedding[:,0][Y1==1], embedding[:,1][Y1==1], color="#c20078", cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax) 
        plt.xlabel('X')                           
        plt.grid(alpha=0.5)

        plt.subplot(144)
        plt.title("Star",size=20)
        plt.scatter(embedding[:,0][Y1==2], embedding[:,1][Y1==2], color="#96f97b" , cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')                        
        plt.grid(alpha=0.5)

        fig.suptitle(r'$N_n$='+str(nn)+r', $M_d$='+str(round(md,2))+r', $M_e$='+str(me.title())+ ", Band:"+band.upper(), size=25, y=1)
        plt.tight_layout()

        plt.savefig(f'{figfile}.png', transparent=True, bbox_inches='tight')
        #plt.savefig(f'{figfile}.pdf', transparent=True, bbox_inches='tight', dpi=450)
        plt.close()

        del fig, Y1, colors, embedding
        # except:
        #     None
                
    return memory, t_time            

def compute_umap_den(file, nn, md, me):
    
    max_size = 4000000000
    night = file.split('/')[2]
    release = file.split('/')[3]
    survey = file.split('/')[4]
    label = file.split('/')[-1][:-4]
    band = label.split('_')[-2]

    relpath = '{}/{}/{}'.format(night,release,survey)

    # try:
    umapfile = f'./reducedcumden/{relpath}/{label}_nn{nn}_md{md}_{me}.csv'
    figfile = f'./figurescumden/{relpath}/{label}_nn{nn}_md{md}_{me}'

    if not(os.path.isdir(f'./reducedcumden/{relpath}')):
        os.makedirs(f'./reducedcumden/{relpath}', exist_ok=True)
    if not(os.path.isdir(f'./figurescumden/{relpath}')):
        os.makedirs(f'./figurescumden/{relpath}', exist_ok=True)
    
    memory, t_time = 0, 0
    
    if ((os.path.isfile(umapfile) == False)):
        fsize = os.stat(file).st_size
        if fsize > max_size:
            print('Max size {}'.format(file))
            # Max_sizefiles.append(file)
        FLUX = list(pd.read_pickle(file)['FLUX'])

        # print('Computing Embedding\n')
        start_time = time.monotonic()
        reducer = umap.UMAP(n_neighbors=nn, min_dist=md, metric=me, random_state=42, low_memory=False)
        reducer.fit(FLUX)
        embedding = reducer.transform(FLUX)
        memory = psutil.Process().memory_info().rss / (1024 * 1024) /1000.0
        t_time = time.monotonic() - start_time
        #print(t_time)
        del reducer, FLUX
        
        den_data = Data(embedding, verbose=False)
        den_data.compute_distances(maxk = 1000)
        den_data.compute_id_2NN()
        den_data.compute_density_kstarNN()  

        df_UMAP = pd.DataFrame()
        df_UMAP['X_UMAP'] = embedding[:,0]
        df_UMAP['Y_UMAP'] = embedding[:,1]
        df_UMAP['LOG_DEN'] = den_data.log_den
        df_UMAP.to_pickle(umapfile)
        del df_UMAP, den_data

        Y1 = np.array(pd.read_pickle(file)['SPECTYPE'])

        classes = np.sort(np.unique(Y1))

        Y1[Y1 == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2
        Y1[Y1 == 'QSO'] = 1
        Y1[Y1 == 'STAR'] = 2

        colors = np.zeros(len(Y1)).astype(str)
        colors[Y1==0] = "#75bbfd"
        colors[Y1==1] = "#c20078"
        colors[Y1==2] = "#96f97b"

        fig = plt.figure(figsize=(20,6))
        xmin = min(embedding[:,0])-1
        xmax = max(embedding[:,0])+1
        ymin = min(embedding[:,1])-1
        ymax = max(embedding[:,1])+1

        size = 2.5
            
        plt.subplot(141)
        plt.title("All",size=20)
        plt.scatter(embedding[:,0], embedding[:,1], c=colors, cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid(alpha=0.5)

        plt.subplot(142)
        plt.title("Galaxy",size=20)
        plt.scatter(embedding[:,0][Y1==0], embedding[:,1][Y1==0], color="#75bbfd", cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)  
        plt.xlabel('X')                         
        plt.grid(alpha=0.5)

        plt.subplot(143)
        plt.title("QSO",size=20)
        plt.scatter(embedding[:,0][Y1==1], embedding[:,1][Y1==1], color="#c20078", cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax) 
        plt.xlabel('X')                           
        plt.grid(alpha=0.5)

        plt.subplot(144)
        plt.title("Star",size=20)
        plt.scatter(embedding[:,0][Y1==2], embedding[:,1][Y1==2], color="#96f97b" , cmap='Paired', s=size)
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')                        
        plt.grid(alpha=0.5)

        fig.suptitle(r'$N_n$='+str(nn)+r', $M_d$='+str(round(md,2))+r', $M_e$='+str(me.title())+ ", Band:"+band.upper(), size=25, y=1)
        plt.tight_layout()

        plt.savefig(f'{figfile}.png', transparent=True, bbox_inches='tight')
        #plt.savefig(f'{figfile}.pdf', transparent=True, bbox_inches='tight', dpi=450)
        plt.close()

        del fig, Y1, colors, embedding
        # except:
        #     None
                
    return memory, t_time      
   
def ifof(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=False ,log=True):
    
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, mm = fof_params[0], fof_params[1]
    
    imagefile = f'{night}_{release}_{survey}_{band}_nn{nn}_md{md}_{me}_ll{ll}_mm{mm}'
    imagepath = f'./figurescum/{night}/{release}/{survey}/outliers'
        
    df_umap = pd.read_pickle(f'./reducedcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./catalogcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./catalogcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    
    aobs = 1.0
    afit = 0.5
        
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    Z = np.array(df_spec['Z'])
    classes = np.unique(SPECTYPE)

    
    SPECTYPE[SPECTYPE == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2 
    SPECTYPE[SPECTYPE == 'QSO'] = 1
    SPECTYPE[SPECTYPE == 'STAR'] = 2

    SPECTYPE = np.array(SPECTYPE.tolist())
    colors = np.zeros(len(SPECTYPE)).astype(str)
    colors[SPECTYPE==0] = "#75bbfd"
    colors[SPECTYPE==1] = "#c20078"
    colors[SPECTYPE==2] = "#96f97b"
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)
    len_flux = len(FLUX[0])

    WAVE = np.array(df_wave['WAVE'])


    ## FOF algorithm to find the islands
    friends = fof(data, ll)

    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min = []
    spec_friends = []
    z_friends = []
    n_members = []
    for f in friends:
        if (len(f)>=mm) & (len(f)<len(data)*0.2):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f)) 
            
    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0
    if save == False:
        return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std
        
        
    if log == True:
        print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,mm))

    if all_island == True:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        
        fig = plt.figure(figsize=(8,8), constrained_layout=True)

        for k, c in enumerate(classes):
            plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=1, label=c, alpha=0.5)
        plt.legend(markerscale=8, fontsize=15)        
        plt.grid(alpha=0.5)
        plt.title('Night={}, Release={}, Survey={} \n Band={}, $L_l$={:.2f}, Islands={}, Outliers={}'.format(night, release.title(), survey.title(), band.upper(),ll,len(friends_min), sum(n_members)),size=20)

        for i in range(len(friends_min)):
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black')
            if log == True:
                pbar.update()
        if log == True:                
            pbar.close()

        xmin = min(XUMAP)-1
        xmax = max(XUMAP)+1
        ymin = min(YUMAP)-1
        ymax = max(YUMAP)+1        
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        if save==True:
            os.makedirs(f'{imagepath}',exist_ok=True)
            plt.savefig('{}/{}.png'.format(imagepath,imagefile), bbox_inches='tight')
            #plt.savefig('{}/{}.pdf'.format(imagepath,imagefile), bbox_inches='tight')
        if plot == True:
            plt.show()
        plt.close()
        del fig

    else:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        for i in range(len(friends_min)):
            fig = plt.figure(figsize=(30,9), constrained_layout=True)
            gs = fig.add_gridspec(3,12)
            ax1 = fig.add_subplot(gs[0:,:4])

            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=1, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{}, Outliers={}'.format(night, release.title(), survey.title(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),size=20)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')            
            
            for k, ii in enumerate(np.random.choice(friends_min[i],3,replace=False)):
                ax2 = fig.add_subplot(gs[k:k+1,4:])

                plt.plot(WAVE,FLUX[ii], alpha=aobs)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(int(np.array(df_spec['EXPID'])[ii]))
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=17)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)


                if (k == 0) or (k == 1):
                    plt.setp(ax2.get_xaxis(), visible=False)
                    plt.setp(ax2.get_xticklabels(), visible=False)
                if (k==1):
                    plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$")

            plt.xlabel('$\lambda$ $[\AA]$')
            if save:
                os.makedirs(f'{imagepath}',exist_ok=True)
                plt.savefig('{}/{}_both_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_both_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            if plot == True:
                plt.show()
            plt.close()
            del fig


            fig, ax = plt.subplots(4,1, figsize=(15,20), sharey=True, sharex=True)
            for k, ii in enumerate(np.random.choice(friends_min[i],4,replace=False)):
                plt.subplot(4,1,k+1)

                plt.plot(WAVE,FLUX[ii], alpha=aobs)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(np.array(df_spec['EXPID'])[ii])
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=14)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)

                plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=18)

            fig.suptitle('Band={}, $L_l$={:.2f}, Island={}/{}, Outliers={}'.format(band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),y=0.92,size=20)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=18)
            if save:
                plt.savefig('{}/{}_spec_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_spec_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            # plt.show()
            plt.close()
            del fig

            fig = plt.figure(figsize=(8,8))
            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=3, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{},  Members={}'.format(night, release.title(), survey.title(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),size=20)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=10, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.tight_layout()
            if save:
                plt.savefig('{}/{}_umap_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_umap_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
#             plt.show()
            plt.close()
            del fig
            if log == True:
                pbar.update()
        if log == True:
            pbar.close()
    
    del data, df_umap, df_spec, df_wave, WAVE, FLUX, SPECTYPE, Z
    #this function return,
    
    return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std
   
   
def ifof_full(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=False, log=True):
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, mm = fof_params[0], fof_params[1]
    
    imagefile = f'{night}_{release}_{survey}_{band}_nn{nn}_md{md}_{me}_ll{ll}_mm{mm}'
    imagepath = f'./demo/{night}/{release}/{survey}/outliers'
    
    df_umap = pd.read_pickle(f'./reducedcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./catalogcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./catalogcum/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    
    aobs = 1.0
    afit = 0.5
    
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    Z = np.array(df_spec['Z'])
    classes = np.unique(SPECTYPE)
    
    SPECTYPE[SPECTYPE == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2 
    SPECTYPE[SPECTYPE == 'QSO'] = 1
    SPECTYPE[SPECTYPE == 'STAR'] = 2
    
    SPECTYPE = np.array(SPECTYPE.tolist())
    colors = np.zeros(len(SPECTYPE)).astype(str)
    colors[SPECTYPE==0] = "#75bbfd"
    colors[SPECTYPE==1] = "#c20078"
    colors[SPECTYPE==2] = "#96f97b"
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)
    len_flux = len(FLUX[0])
    
    WAVE = np.array(df_wave['WAVE'])
    
    ## FOF algorithm to find the islands
    friends = fof(data, ll)

    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min  = []
    spec_friends = []
    z_friends    = []
    n_members    = []
    for f in friends:
        if (len(f)>=mm) & (len(f)<len(data)*0.2):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f)) 

    if log == True:
        print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,mm))           
           
    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0
    if save == False:
        return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std

    if all_island == True:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        
        fig = plt.figure(figsize=(8,8), constrained_layout=True)

        for k, c in enumerate(classes):
            plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=1, label=c, alpha=0.5)
        plt.legend(markerscale=8, fontsize=15)        
        plt.grid(alpha=0.5)
        plt.title('Night={}, Release={}, Survey={} \n Band={}, $L_l$={:.2f}, Islands={}, Outliers={}'.format(night, release.title(), survey.title(), band.upper(),ll,len(friends_min), sum(n_members)),size=20)

        for i in range(len(friends_min)):
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black')
            if log == True:
                pbar.update()
        if log == True:                
            pbar.close()

        xmin = min(XUMAP)-1
        xmax = max(XUMAP)+1
        ymin = min(YUMAP)-1
        ymax = max(YUMAP)+1        
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        if save==True:
            os.makedirs(f'{imagepath}',exist_ok=True)
            plt.savefig('{}/{}.png'.format(imagepath,imagefile), bbox_inches='tight')
            #plt.savefig('{}/{}.pdf'.format(imagepath,imagefile), bbox_inches='tight')
        if plot == True:
            plt.show()
        plt.close()
        del fig
        
        pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        for i in range(len(friends_min)):
            for ii in friends_min[i]:
                fig = plt.figure(figsize=(15,5))
                plt.plot(WAVE,FLUX[ii], alpha=aobs)
                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                     +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                     +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                     +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                     +", "+"EXPID="+str(np.array(df_spec['EXPID'])[ii])
                     +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                     +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                     , fontsize=14)
                #plt.legend(fontsize=12)
                plt.grid(alpha=0.5)
                plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=18)
                plt.xlabel('$\lambda$ $[\AA]$', fontsize=18)
                
                fig.suptitle('Band={}, $L_l$={:.2f}, Island_ID={}, Outlier_ID={}'.format(band.upper(),ll,i,ii),y=1.01,size=15)
                plt.savefig(f'{imagepath}/{imagefile}_{i}_{ii}_out.png', bbox_inches='tight')
                plt.close()
                del fig
            pbar.update()
        pbar.close()
        
    return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std
   
   
def ifof_den(obs_params, reduc_params, fof_params, plot=False, save=False, all_island=False, log=True):
    
    night, release, survey, band = obs_params[0], obs_params[1], obs_params[2], obs_params[3]
    nn, md, me = reduc_params[0], reduc_params[1], reduc_params[2]
    ll, mm = fof_params[0], fof_params[1]
    
    imagefile = f'{night}_{release}_{survey}_{band}_nn{nn}_md{md}_{me}_ll{ll}_mm{mm}'
    imagepath = f'./finalfigures/{night}/{release}/{survey}/outliers'
    
    os.makedirs(imagepath, exist_ok=True)
    
    df_umap = pd.read_pickle(f'./finalreduced/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec_nn{nn}_md{md}_{me}.csv')
    df_spec = pd.read_pickle(f'./finalcatalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_spec.csv')
    df_wave = pd.read_pickle(f'./finalcatalog/{night}/{release}/{survey}/{night}_{release}_{survey}_{band}_wave.csv')
    
    #print(df_umap)
    
    XUMAP = np.array(df_umap['X_UMAP'])
    YUMAP = np.array(df_umap['Y_UMAP'])
    data = np.array([XUMAP,YUMAP], dtype=float).T
    
    #print(data)
    
    aobs = 1.0
    afit = 0.5
        
    Z = np.array(df_spec['Z'])
    
    SPECTYPE = np.array(df_spec['SPECTYPE'])
    classes = np.unique(SPECTYPE)
    SPECTYPE[SPECTYPE == 'GALAXY'] = 0  # GALAXY=0  -   QSO=1    -  STAR=2 
    SPECTYPE[SPECTYPE == 'QSO'] = 1
    SPECTYPE[SPECTYPE == 'STAR'] = 2
    SPECTYPE = np.array(SPECTYPE, dtype=int)
    
    colors = np.zeros(len(SPECTYPE)).astype(str)
    colors[SPECTYPE==0] = "#75bbfd"
    colors[SPECTYPE==1] = "#c20078"
    colors[SPECTYPE==2] = "#96f97b"
    
    FLUX = np.array(df_spec['FLUX'])
    size = len(FLUX)
    len_flux = len(FLUX[0])

    WAVE = np.array(df_wave['WAVE'])

    #-------Compute density
    data_den = Data(data, verbose=False)
    data_den.compute_density_kstarNN()
    density = data_den.log_den

    ## FOF algorithm to find the islands
    friends = fof(data, ll)
    #print(len(friends))
    # Apply a filter on the FOF:    friends>number_friends &  friends<data/# to remove big clusters
    friends_min = []
    spec_friends = []
    z_friends = []
    n_members = []  
    for f in friends:
        if (len(f)>=mm) & (len(f)<len(data)*0.2) & (np.average(density[f])<-6.5):
            friends_min.append(f)
            spec_friends.append(SPECTYPE[f])
            z_friends.append(Z[f])
            n_members.append(len(f))
            
    if len(n_members)!=0:
        mean = np.mean(n_members)
        std = np.std(n_members)
    else:
        mean = 0
        std = 0
       
    if log == True:
        print('{0} islands for ll {1:.2} with min {2} members!'.format(len(friends_min),ll,mm))
        print('{} outliers identified!'.format(sum(n_members)))       
       
    if save == False:
        return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std
        
       
    if all_island == True:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        
        fig = plt.figure(figsize=(8,8), constrained_layout=True)

        for k, c in enumerate(classes):
            plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=1, label=c, alpha=0.5)
        plt.legend(markerscale=8, fontsize=15)        
        plt.grid(alpha=0.5)
        plt.title('Night={}, Release={}, Survey={} \n Band={}, $L_l$={:.2f}, Islands={}, Outliers={}'.format(night, release.title(), survey.title(), band.upper(),ll,len(friends_min), sum(n_members)),size=20)

        for i in range(len(friends_min)):
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black')
            if log == True:
                pbar.update()
        if log == True:                
            pbar.close()

        xmin = min(XUMAP)-1
        xmax = max(XUMAP)+1
        ymin = min(YUMAP)-1
        ymax = max(YUMAP)+1        
        plt.xlim(xmin,xmax)
        plt.ylim(ymin,ymax)
        plt.xlabel('X')
        plt.ylabel('Y')
        if save==True:
            os.makedirs(f'{imagepath}',exist_ok=True)
            plt.savefig('{}/{}.png'.format(imagepath,imagefile), bbox_inches='tight')
            #plt.savefig('{}/{}.pdf'.format(imagepath,imagefile), bbox_inches='tight')
        if plot == True:
            plt.show()
        plt.close()
        del fig

    else:
        if log == True:
            pbar =  tqdm(total=len(friends_min), desc=f'{night}/{release}/{survey}->{band}')
        for i in range(len(friends_min)):
            fig = plt.figure(figsize=(30,9), constrained_layout=True)
            gs = fig.add_gridspec(3,12)
            ax1 = fig.add_subplot(gs[0:,:4])

            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=1, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{}, Outliers={}'.format(night, release.title(), survey.title(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),size=20)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=5, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')            
            
            for k, ii in enumerate(np.random.choice(friends_min[i],3,replace=False)):
                ax2 = fig.add_subplot(gs[k:k+1,4:])

                plt.plot(WAVE,FLUX[ii], alpha=aobs)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(int(np.array(df_spec['EXPID'])[ii]))
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=17)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)


                if (k == 0) or (k == 1):
                    plt.setp(ax2.get_xaxis(), visible=False)
                    plt.setp(ax2.get_xticklabels(), visible=False)
                if (k==1):
                    plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$")

            plt.xlabel('$\lambda$ $[\AA]$')
            if save:
                os.makedirs(f'{imagepath}',exist_ok=True)
                plt.savefig('{}/{}_both_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_both_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            if plot == True:
                plt.show()
            plt.close()
            del fig


            fig, ax = plt.subplots(4,1, figsize=(15,20), sharey=True, sharex=True)
            for k, ii in enumerate(np.random.choice(friends_min[i],4,replace=False)):
                plt.subplot(4,1,k+1)

                plt.plot(WAVE,FLUX[ii], alpha=aobs)

                plt.title(str(np.array(df_spec['SPECTYPE'])[ii])
                          +", "+"TILEID="+str(np.array(df_spec['TILEID'])[ii])
                          +", "+"PETAL="+str(np.array(df_spec['PETAL_LOC'])[ii])
                          +", "+"TARGETID="+str(int(np.array(df_spec['TARGETID'])[ii]))
                          +", "+"EXPID="+str(np.array(df_spec['EXPID'])[ii])
                          +", "+"FIBER="+str(int(np.array(df_spec['FIBER'])[ii]))
                          +", "+"Z="+str(round(np.array(df_spec['Z'])[ii],3))
                          , fontsize=14)
                # plt.legend(fontsize=12)
                plt.grid(alpha=0.5)

                plt.ylabel("Flux $10^{-17}[erg/s\,cm^2\,\AA]$", fontsize=18)

            fig.suptitle('Band={}, $L_l$={:.2f}, Island={}/{}, Outliers={}'.format(band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),y=0.92,size=20)

            plt.xlabel('$\lambda$ $[\AA]$', fontsize=18)
            if save:
                plt.savefig('{}/{}_spec_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_spec_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
            # plt.show()
            plt.close()
            del fig

            fig = plt.figure(figsize=(8,8))
            for k, c in enumerate(classes):
                plt.scatter(XUMAP[SPECTYPE==k], YUMAP[SPECTYPE==k], c=colors[SPECTYPE==k], cmap='Paired', s=3, label=c, alpha=0.5)

            plt.legend(markerscale=8, fontsize=15)        
            plt.grid(alpha=0.5)
            plt.title('Night={}, Release={}, Survey={} \n Band={}, L$_l$={:.2f}, Island={}/{},  Members={}'.format(night, release.title(), survey.title(), band.upper(),ll,i+1,len(friends_min),len(friends_min[i])),size=20)
            plt.scatter(XUMAP[friends_min[i]],YUMAP[friends_min[i]], s=10, c='black')
            xmin = min(XUMAP)-1
            xmax = max(XUMAP)+1
            ymin = min(YUMAP)-1
            ymax = max(YUMAP)+1        
            plt.xlim(xmin,xmax)
            plt.ylim(ymin,ymax)
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.tight_layout()
            if save:
                plt.savefig('{}/{}_umap_{}.png'.format(imagepath,imagefile,i+1), bbox_inches='tight')
                #plt.savefig('{}/{}_umap_{}.pdf'.format(imagepath,imagefile,i+1), bbox_inches='tight')
#             plt.show()
            plt.close()
            del fig
            if log == True:
                pbar.update()
        if log == True:
            pbar.close()
    
    del data, df_umap, df_spec, df_wave, WAVE, FLUX, SPECTYPE, Z
    #this function return,
    
    return friends_min, spec_friends, z_friends, len(friends_min), size, len_flux, n_members, sum(n_members), sum(n_members)*100/size, mean, std